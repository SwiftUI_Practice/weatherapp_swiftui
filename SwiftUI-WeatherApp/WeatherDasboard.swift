//
//  WeatherDasboard.swift
//  SwiftUI-WeatherApp
//
//  Created by Amit Pandey on 08/03/2024.
//

import SwiftUI

struct WeatherDasboard: View {
    
    @State private var isNight: Bool = false
    
    var body: some View {
        ZStack {
            LinearGradient(colors: self.isNight ? [.black, .gray] : [.blue, .white], startPoint: .topLeading, endPoint: .bottomTrailing)
                .ignoresSafeArea()
            
            VStack {
                
                Text("Cupertino, CA")
                    .foregroundColor(.white)
                    .font(.system(size: 40, weight: .bold))
                    .padding(.top, 10)
                
                VStack(spacing: 10) {
                    
                    Image(systemName: self.isNight ? "moon.dust.fill" : "cloud.sun.fill")
                        .resizable()
                        .renderingMode(.original)
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 200, height: 200)
                    
                    Text("76°")
                        .font(.system(size: 70, weight: .bold))
                        .foregroundColor(.white)
                        .padding(.top, -10)
                    
                }
                
                HStack(spacing: 20) {
                    
                    DayWeather(day: "TUE", weatherIcon: "cloud.sun.fill", temprature: "65°")
                    DayWeather(day: "WED", weatherIcon: "sun.max.fill", temprature: "70°")
                    DayWeather(day: "THU", weatherIcon: "wind", temprature: "76°")
                    DayWeather(day: "FRI", weatherIcon: "sun.horizon.fill", temprature: "60°")
                    DayWeather(day: "SAT", weatherIcon: "moon.stars.fill", temprature: "55°")
                    
                }.padding(.top, 30)
                
                Spacer()
                
                Button(action: {
                    NSLog("Button Tapped!")
                    
                    self.isNight.toggle()
                    
                }, label: {
                    Text("Change Time of Day")
                        .font(.system(size: 20, weight: .medium))
                        .padding(.horizontal, 30)
                        .padding(.vertical, 10)
                        .background(.white)
                        .clipShape(RoundedRectangle(cornerSize: CGSize(width: 10, height: 10)))
                        
                })
                
                Spacer()
            }
        }
    }
}

struct DayWeather: View {
    
    var day: String
    var weatherIcon: String
    var temprature: String
    
    var body: some View {
        VStack {
            
            Text(day)
                .foregroundStyle(.white)
                .font(.system(size: 20, weight: .medium))
            
            Image(systemName: weatherIcon)
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 50, height: 50)
            
            Text(temprature)
                .foregroundStyle(.white)
                .font(.system(size: 20, weight: .bold))
        }
    }
}


#Preview {
    WeatherDasboard()
}
